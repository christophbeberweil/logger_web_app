#[macro_use]
extern crate nickel;
use chrono::Local;


use std::{
    fs::{self, File, OpenOptions},
    io::{LineWriter, Write}, env::{self, VarError}, path::Path,
};

use nickel::{HttpRouter, Nickel, Request, Response, MiddlewareResult, QueryString};


fn main() {

    // parse location of the logfile
    let default_path = String::from("./storage/data.txt");
    // read environment variable
    let path_option = env::var("LOGGER_WEB_APP_FILEPATH");
    let path = calculate_usable_string(default_path, path_option);
    println!{"Logfile: {}", path};
    
    // prepare and start web server
    let mut server = Nickel::new();
    let mut router = Nickel::router();
    router.get("**", middleware!(handle_request(&path)));

    server.utilize(api_token_authentication);
    server.utilize(logger_fn);

    server.utilize(router);
    server
        .listen("0.0.0.0:3000")
        .expect("Could not start server");
}



/// calculates an owned String from an Owned string or a Result<String, VarError>
fn calculate_usable_string(default_value: String, string_option: Result<String, VarError>) -> String{
    match string_option{
        Ok(value) => value,
        Err(_) => default_value,
    }

}



fn handle_request(raw_path: &String) -> String {
    let path = Path::new(raw_path);

    match path.parent() {
        Some(parent) => fs::create_dir_all(parent).expect("could not create directories"),
        None => {}
    }

    let now = Local::now();

    let file: File = OpenOptions::new()
        .create(true)
        .write(true)
        .append(true)
        .open(path)
        .expect("Could not open file");
    let mut file = LineWriter::new(file);

    let now_formatted = now.to_rfc3339();

    let _ = file.write_all(now_formatted.as_bytes()) else {
        return format!("Could not write date to file")
    };
    let _ = file.write_all(b"\n") else {
        return format!("Could not write date to file")
    };
   
    format!("Logged: {}", now)
}



// middleware functions

fn logger_fn<'a>(_req: &mut Request, res: Response<'a>) -> MiddlewareResult<'a> {
    //println!("logging request from logger fn: {:?}", req.origin.uri);
    res.next_middleware()
}

fn api_token_authentication<'a>(req: &mut Request, res: Response<'a>) -> MiddlewareResult<'a> {
    


    let configured_token = env::var("LOGGER_WEB_APP_API_TOKEN");
    match configured_token {
        Ok(configured_token) => {
            
            if let Some(token) = req.query().get("token"){
                if token != configured_token{
                    res.send(format!("Not Authenticated"))
                } else {
                    res.next_middleware()
                }
            } else {
                res.send(format!("no token submitted"))
            }
            
        },
        Err(_e) => {
            res.next_middleware()
        },
    }
}