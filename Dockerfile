# Dockerfile for creating a statically-linked Rust application using docker's
# multi-stage build feature. This also leverages the docker build cache to avoid
# re-downloading dependencies if they have not changed.
FROM rust:1.66.1 AS build
WORKDIR /usr/src

# Download the target for static linking.
RUN rustup target add x86_64-unknown-linux-musl

# Create a dummy project and build the app's dependencies.
# If the Cargo.toml or Cargo.lock files have not changed,
# we can use the docker build cache and skip these (typically slow) steps.
RUN USER=root cargo new logger_web_app
WORKDIR /usr/src/logger_web_app
COPY Cargo.toml Cargo.lock ./
RUN cargo build --release

# Copy the source and build the application.
COPY src ./src
RUN cargo install --target x86_64-unknown-linux-musl --path .

# Copy the statically-linked binary into a scratch container.
FROM debian

RUN ls -lah

RUN mkdir /app
RUN mkdir /app/storage
RUN chown -R 1000:1000 /app 
WORKDIR /app
COPY --from=build /usr/local/cargo/bin/logger_web_app /app/
USER 1000
CMD ["./logger_web_app"]
